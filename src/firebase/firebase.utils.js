import firebase from 'firebase/app';
import 'firebase/firestore';
import 'firebase/auth';


const config = {
  apiKey: "AIzaSyDsqPlGS47DvAcd57C5JDtf7SYoZxZ2zr0",
  authDomain: "crowndb-4bc0b.firebaseapp.com",
  databaseURL: "https://crowndb-4bc0b.firebaseio.com",
  projectId: "crowndb-4bc0b",
  storageBucket: "crowndb-4bc0b.appspot.com",
  messagingSenderId: "828300885205",
  appId: "1:828300885205:web:22ccf90634d2d9bdc7765c",
  measurementId: "G-RL7D0MN074"
};

export const createUserProfileDocument = async (userAuth, additionalData) => {
  if (!userAuth) return;

  const userRef = firestore.doc(`users/${userAuth.uid}`);

  const snapShot = await userRef.get();

  if (!snapShot.exists) {
    const { displayName, email } = userAuth;
    const createdAt = new Date();

    try {
      await userRef.set({
        displayName,
        email,
        createdAt,
        ...additionalData
      })
    } catch (error) {
      console.log('error creating user', error.message);
    }
  }
  
  return userRef;
}



firebase.initializeApp(config);

export const auth = firebase.auth();
export const firestore = firebase.firestore();

const provider = new firebase.auth.GoogleAuthProvider();
provider.setCustomParameters({ prompt: 'select_account' });
export const signInWithGoogle = () => auth.signInWithPopup(provider);

export default firebase;